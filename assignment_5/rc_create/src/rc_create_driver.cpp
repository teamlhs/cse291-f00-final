#include "ros/ros.h"
#include "rc_create/Control.h"
#include <iostream>
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h>
#include <termios.h>
#include <time.h>   // time calls

using namespace std;

/* Define the serial port path */
#define CREATE_SERIAL_PORT "/dev/ttyUSB0"
/* Define baudrate */
#define CREATE_SERIAL_BRATE B115200

static int fd;

// Example for Forward Movement
void forward() {
    int forward[] = {137, 0, 100, 128, 0};
    int wd;
    wd =  write(fd, forward, 5);
    cout << "FORWARD Written";
}

void backward() {
    int backward[] = {137, 255, 156, 128, 0};
    int wd;
    wd =  write(fd, backward, 5);
    cout << "BACKWARD Written";
}

void left() {
    int left[] = {137, 0, 100, 0, 1};
    int wd;
    wd =  write(fd, left, 5);
    cout << "LEFT Written";
}

void right() {
    int right[] = {137, 0, 100, 255, 255};
    int wd;
    wd =  write(fd, right, 5);
    cout << "RIGHT Written";
}

void stop() {
    int stop[] = {137, 0, 0, 128, 0};
    int wd;
    wd =  write(fd, stop, 5);
    cout << "STOP Written";
}

void song() {
    int song[] = {140, 3, 1, 64, 16, 141, 3};
    int wd;
    wd =  write(fd, song, 7);
    cout << "SONG Written";
}

string mode;

bool controlCallback(rc_create::Control::Request &req, rc_create::Control::Response &res) {
    mode=req.mode;
    if (mode=="FORWARD") {
        forward();
    } else if (mode=="BACKWARD") {
        backward();
    } else if (mode=="LEFT") {
        left();
    } else if (mode=="RIGHT") {
        right();
    } else if (mode=="STOP") {
        stop();
    } else if (mode=="SONG") {
        song();
    } else {	
        res.message="fail! Request not found";
        return false;
    }	
    res.message="success!";
    return true;
}


int create_init() {
    ////////////////
    // Initialize //
    ////////////////

    fd = open(CREATE_SERIAL_PORT, O_RDWR | O_NOCTTY | O_NDELAY );


    // can't open port
    if (fd == -1) {
        cout << "Error opening port\n";
        return -1;
    }
    // open successful
    else {
        cout << "Serial port opened with status: " << fd << endl;
        fcntl(fd, F_SETFL, 0);
    }
    // configure port
    struct termios portSettings;
    tcgetattr(fd, &portSettings);


    if(cfsetispeed(&portSettings, CREATE_SERIAL_BRATE) != 0) {
        cout << "Failed setting baud rate for input";
    }
    if(cfsetospeed(&portSettings, CREATE_SERIAL_BRATE) != 0) {
        cout << "Failed setting baud rate for output";
    }


    //set parity bits
    portSettings.c_cflag &= ~PARENB;
    portSettings.c_cflag &= ~CSTOPB;
    portSettings.c_cflag &= ~CSIZE;
    portSettings.c_cflag |= CS8;
    cfmakeraw(&portSettings);


    if(tcsetattr(fd, TCSANOW, &portSettings) != 0) {
        cout << "Failed pushing port settings.\n";
        return fd;
    }

    // Running iRobot create

    int initsafe[] = {128, 131};
    int initfull[] = {128, 132}; 

    int wd;
    wd = write(fd, initfull, 2);
    wd = write(fd, initfull, 2);
    wd = write(fd, initfull, 2);
}



int main(int argc, char **argv) {
    /*************************************
     *
     * FILL IN YOUR FUNCTIONALITY HERE.
     *
     *************************************/
    ros::init(argc, argv, "rc_create_driver");

    ros::NodeHandle node;

    ros::ServiceServer service = node.advertiseService("mode", controlCallback);

    create_init();

    while(ros::ok()) {
        ros::spin();
    }

    return 0;
}
