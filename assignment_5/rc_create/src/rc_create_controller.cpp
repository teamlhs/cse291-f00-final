#include "ros/ros.h"
#include <cstdio>
#include "ncurses.h"
#include "rc_create/Control.h"
#include "std_srvs/Empty.h"

using namespace std;

int main(int argc, char **argv) {
    ros::init(argc, argv, "rc_create_controller");
    ros::NodeHandle n;

    // Setup client to exit session.
    ros::ServiceClient exitClient = n.serviceClient<std_srvs::Empty>("exit");

    // Setup client to change mode.
    ros::ServiceClient modeClient = n.serviceClient<rc_create::Control>("mode");

    string mode = "STOP";

    initscr();
    cbreak();
    noecho();
    while(ros::ok()) {
        char ch = getch();
        if (ch == 27) { // skip esc
            getch(); // skip [
            switch (getch()) {
            case 'A':
                mode = "FORWARD";
                break;
            case 'B':
                mode = "BACKWARD";
                break;
            case 'C':
                mode = "RIGHT";
                break;
            case 'D':
                mode = "LEFT";
                break;
            }
        } else if (ch == ' ') {
            mode = "STOP";
        } else if (ch == 's') {
            mode = "SONG";
        } else if (ch == 'e') {
            std::cout << "Exiting..." << std::endl;

            // Call exit service.
            std_srvs::Empty exitSrv;
            exitClient.call(exitSrv);

            return 0;
        } else {
            fprintf(stderr, "\nUnrecognizable keystorke\n");
            continue;
        }

        // Setup the service request.
        rc_create::Control srv;
        srv.request.mode = mode;

        printf("%s\n", mode.c_str());

        // Call the service request.
        if (modeClient.call(srv)) {
            fprintf(stderr, "\nSuccessful mode %s\n", mode.c_str());
        } else {
            fprintf(stderr, "\nSomething goes wrong for mode %s\n", mode.c_str());
            return 1;
        }
    }

    endwin();
}
