#include "ros/ros.h"
#include "motion_detector/Mode.h"
#include "std_srvs/Empty.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;
using namespace cv;

image_transport::Subscriber imageSub;
image_transport::Publisher imagePub;

static const string OPENCV_WINDOW = "Image window";
string mode = "RAW VIDEO";

Ptr<BackgroundSubtractor> mog2;

bool first = true;
bool leave = true;
Mat preImage, curImage, outImage;
Mat preGImage, curGImage, outGImage;

const int THRESH = 100;
const double MIN_AREA = 3600;
const double EPS = 1;
int dilation_elem = 0;
int dilation_size = 0;

bool isZero(double x) {
   return x > -EPS and x < EPS;
}

void drawOptFlowMap (const Mat& flow, Mat& cflowmap, int step, const Scalar& color) {
   for(int y = 0; y < cflowmap.rows; y += step) {
      for(int x = 0; x < cflowmap.cols; x += step) {
         const Point2f& fxy = flow.at< Point2f>(y, x);
         if (isZero(fxy.x) and isZero(fxy.y)) {
            // do nothing
         } else {
            circle(cflowmap, Point(cvRound(x), cvRound(y)), 25, 255, -1);
         }
      }
   }
}

void drawBoundingBox(Mat& colorMat, const Mat& mat) {
   Mat threshold_output;
   vector<vector<Point>> contours;
   vector<Vec4i> hierarchy;

   // detect edges
   threshold(mat, threshold_output, THRESH, 255, THRESH_BINARY);

   // dilate
   //Mat element = getStructuringElement(MORPH_RECT, Size(2*dilation_size + 1, 2*dilation_size+1), Point(dilation_size, dilation_size));
   //dilate(threshold_output, threshold_output, element);

   // find contours
   findContours(threshold_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

   vector<Rect> boundRect;

   // approximat contours to rects
   for (int i = 0; i < contours.size(); i++) { 
      if (contourArea(contours[i]) < MIN_AREA) {
         // do nothing
      } else {
         boundRect.push_back(boundingRect(contours[i]));
      }
   }

   // draw rects
   for (int i = 0; i < boundRect.size(); i++) {
      Scalar color = Scalar(255, 0, 0);
      rectangle(colorMat, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0);
   }
}

void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
   cv_bridge::CvImagePtr cv_ptr;
   try {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
   } catch (cv_bridge::Exception& e) {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
   }

   if (first == false) {
      preImage = curImage.clone();
      cvtColor(preImage, preGImage, CV_BGR2GRAY);
   }
   curImage = cv_ptr->image.clone();
   cvtColor(curImage, curGImage, CV_BGR2GRAY);

   if (mode == "RAW VIDEO") {
      leave = true;
      // nothing to be done
   } else if (first == false and mode == "OPTICAL FLOW") {
      leave = true;
      calcOpticalFlowFarneback(preGImage, curGImage, outGImage, 0.5, 3, 15, 3, 5, 1.2, 0); // 0.4, 1, 12, 2, 8, 1.2, 0
      outImage = Mat::zeros(curImage.rows, curImage.cols, CV_8UC1);
      drawOptFlowMap(outGImage, outImage, 40, CV_RGB(0, 255, 0));
      drawBoundingBox(cv_ptr->image, outImage);
   } else if (mode == "MOG2") {
      if (leave == true) {
         leave = false;
         mog2 = new BackgroundSubtractorMOG2(1, 16, false);
         mog2->setDouble("fTau", 0.5);
      } else {
         // do nothing
      }
      Mat curBGImage;
      mog2->operator()(curImage, outImage);
      drawBoundingBox(cv_ptr->image, outImage);
   } else {
      ROS_INFO("Not exsting mode"); 
   }

   if (first == true) {
      first = false;
   }

   // Output modified video stream
   imagePub.publish(cv_ptr->toImageMsg());
}

bool exitCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
   ros::shutdown();
   return true;
}

bool modeCallback(motion_detector::Mode::Request &req, motion_detector::Mode::Response &res) {
   mode = req.m;
   return true;
}

int main(int argc, char **argv) {
   ros::init(argc, argv, "motion_detector_node");
   ros::NodeHandle n;

   // Setup server to exit session.
   ros::ServiceServer exitServer = n.advertiseService("exit", exitCallback);

   // Setup server to change mode.
   ros::ServiceServer modeServer = n.advertiseService("mode", modeCallback);

   namedWindow(OPENCV_WINDOW);

   // Subscrive to input video feed and publish output video feed
   image_transport::ImageTransport it(n);
   imageSub = it.subscribe("/usb_cam/image_raw", 1, imageCallback);
   imagePub = it.advertise("/image_converter/output_video", 1);

   ros::spin();

   destroyWindow(OPENCV_WINDOW);

   return 0;
}
