#include "ros/ros.h"
#include "motion_detector/Mode.h"
#include "std_srvs/Empty.h"

int main(int argc, char **argv) {
   ros::init(argc, argv, "motion_detector_keyboard");
   ros::NodeHandle n;

   // Setup client to exit session.
   ros::ServiceClient exitClient = n.serviceClient<std_srvs::Empty>("exit");

   // Setup client to change mode.
   ros::ServiceClient modeClient = n.serviceClient<motion_detector::Mode>("mode");

   std::string mode = "RAW VIDEO";
   // Optical Flow
   // MOG2

   while(ros::ok()) {
      std::string input;

      // Clear screen.
      std::cout << "\033[2J\033[1;1H";

      // Print menu.
      std::cout << "MAIN MENU" << std::endl;
      std::cout << "---------------------------------" << std::endl;
      std::cout << "Current mode: " << mode << std::endl;
      std::cout << "---------------------------------" << std::endl;
      std::cout << "Enter Mode (r/o/m) or Exit (e): " << std::endl;
      std::cin >> input;
      std::cout << std::endl;

      if (input == "r") {
         mode = "RAW VIDEO";
      } else if (input == "o") {
         mode = "OPTICAL FLOW";
      } else if (input == "m") {
         mode = "MOG2";
      } else if (input == "e"){
         std::cout << "Exiting..." << std::endl;

         // Call exit service.
         std_srvs::Empty exitSrv;
         exitClient.call(exitSrv);

         return 0;
      } else {
         std::cout << "'" << input << "' is not a valid mode." << std:: endl;
         std::cout << "---------------------------------" << std::endl;

         // Continue message.
         std::cout << "Press any key to continue.";
         getchar();
         getchar();
         continue;
      }

      // Setup the service request.
      motion_detector::Mode srv;
      srv.request.m = mode;

      // Call the service request.
      if (modeClient.call(srv)) {
         std::cout << "Successively changed mode" << std::endl;
      } else {
         std::cout << "Failed to call service mode";
         return 1;
      }

      std::cout << "Mode set to " << mode << std::endl;
      std::cout << "---------------------------------" << std::endl;

      // Continue message.
      std::cout << "Press any key to continue.";
      getchar();
      getchar();
      continue;
   }

   return 0;
}
