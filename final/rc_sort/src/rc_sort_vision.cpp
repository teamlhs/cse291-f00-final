#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/String.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include "rc_sort/Command.h"
#include "rc_sort/Collide.h"
#include "unordered_map"
//#include "create/create.h"

using namespace cv;
using namespace std;

image_transport::Subscriber imageSub;
image_transport::Publisher imagePub;

ros::ServiceClient commandClient;
ros::ServiceServer collideServer;

unordered_map<string,bool> umap;

Scalar color = Scalar(255, 0, 0);
Scalar redLow = Scalar(0, 130, 30);
Scalar redHigh = Scalar(14, 235, 230);
//Scalar yellowLow = Scalar(26, 30, 90);
//Scalar yellowHigh = Scalar(32, 235, 240);
Scalar purpleLow = Scalar(150, 30, 60);
Scalar purpleHigh = Scalar(179, 200, 220);
Scalar blueLow = Scalar(105, 30, 50);
Scalar blueHigh = Scalar(119, 180, 250);
Scalar greenLow = Scalar(30, 30, 80);
Scalar greenHigh = Scalar(43, 150, 240);

const int labelFrame=30;
const int changeFrame=10;
int currentFrame=0;

rc_sort::Command command;

int status=0;

string mode="undefined";
//string mode="red";

const int leftBound=240;
const int rightBound=400;
const int upperBound=240;

Mat HSV, threshold_image_red, threshold_image_purple, threshold_image_blue, threshold_image_green;
vector<vector<Point>> contours, contours_red, contours_purple, contours_blue, contours_green;
vector<Vec4i> hierarchy;
Rect boundRect;

static const string OPENCV_WINDOW = "Image window";

const int THRESH = 100;
const double MIN_AREA = 200;

bool collideCallback(rc_sort::Collide::Request &req, rc_sort::Collide::Response &res){
    if(req.collide and status==1){
        ROS_INFO("I get the %s object!\n",mode.c_str());
        status=2;
    }
    return true;
}

void undefined(Mat& ptr, Mat& HSV) {
    bool complete = false;
    complete |= umap["red"];
    complete |= umap["purple"];
    complete |= umap["blue"];
    complete |= umap["green"];
    if (complete == false) {
	status=6;
	return;
    }

    inRange(HSV, redLow, redHigh, threshold_image_red);
    inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
    inRange(HSV, blueLow, blueHigh, threshold_image_blue);
    inRange(HSV, greenLow, greenHigh, threshold_image_green);

    //morphological opening (remove small objects from the foreground)
    //  erode(cv_output->image, cv_output->image, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)) );
    //  dilate(cv_output->image, cv_output->image, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)) ); 

    //morphological closing (fill small holes in the foreground)
    //  dilate(cv_output->image, cv_output->image, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)) ); 
    //  erode(cv_output->image, cv_output->image, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)) );

    // find contours
    findContours(threshold_image_red, contours_red, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    findContours(threshold_image_purple, contours_purple, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    findContours(threshold_image_blue, contours_blue, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    findContours(threshold_image_green, contours_green, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    double maxArea_red=0, maxArea_purple=0, maxArea_blue=0, maxArea_green=0;

    // approximate contours to rects
    for (int i = 0; i < contours_red.size(); i++) { 
        if (contourArea(contours_red[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours_red[i])>maxArea_red){
                Moments mom_red=moments(contours_red[i],true);
                if (mom_red.m01/mom_red.m00>upperBound){
                    maxArea_red=contourArea(contours_red[i]);
                }
            }
        }
    }

    for (int i = 0; i < contours_purple.size(); i++) { 
        if (contourArea(contours_purple[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours_purple[i])>maxArea_purple){
                Moments mom_purple=moments(contours_purple[i],true);
                if (mom_purple.m01/mom_purple.m00>upperBound){
                    maxArea_purple=contourArea(contours_purple[i]);
                }
            }
        }
    }

    for (int i = 0; i < contours_blue.size(); i++) { 
        if (contourArea(contours_blue[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours_blue[i])>maxArea_blue){
                Moments mom_blue=moments(contours_blue[i],true);
                if (mom_blue.m01/mom_blue.m00>upperBound){
                    maxArea_blue=contourArea(contours_blue[i]);
                }
            }
        }
    }

    for (int i = 0; i < contours_green.size(); i++) { 
        if (contourArea(contours_green[i]) < MIN_AREA) {
            // do nothing
        } 
        else {
            if (contourArea(contours_green[i])>maxArea_green){
                Moments mom_green=moments(contours_green[i],true);
                if (mom_green.m01/mom_green.m00>upperBound){
                    maxArea_green=contourArea(contours_green[i]);
                }
            }
        }
    }

    vector<double> areas;
    areas.push_back(maxArea_red);
    areas.push_back(maxArea_purple);
    areas.push_back(maxArea_blue);
    areas.push_back(maxArea_green);

    vector<string> names;
    names.push_back("red");
    names.push_back("purple");
    names.push_back("blue");
    names.push_back("green");

    double maxArea = 0;
    int idx = -1;

    for (int i = 0; i < 4; i++) {
        if (areas[i] > maxArea and umap[names[i]] == true) {
            maxArea = areas[i];
            idx = i;
        } 
    }

    if (maxArea==0){
        command.request.command="LEFT";
        commandClient.call(command);
    } else {
        ++currentFrame;
        if (currentFrame==changeFrame){
            mode = names[idx];
            ROS_INFO("I find a %s object!\n", mode.c_str());
            currentFrame=0;
        }
    }
}

void status0(Mat& ptr, Mat& HSV) {
    if (mode=="red"){
        inRange(HSV, redLow, redHigh, threshold_image_red);
        findContours(threshold_image_red, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="purple"){
        inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
        findContours(threshold_image_purple, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="blue"){
        inRange(HSV, blueLow, blueHigh, threshold_image_blue);
        findContours(threshold_image_blue, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="green"){
        inRange(HSV, greenLow, greenHigh, threshold_image_green);
        findContours(threshold_image_green, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    }

    double maxArea=0;

    // approximate contours to rects
    for (int i = 0; i < contours.size(); i++) { 
        if (contourArea(contours[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours[i])>maxArea){
                Moments mom;
                mom=moments(contours[i],true);
                if (mom.m01/mom.m00>upperBound){
                    maxArea=contourArea(contours[i]);
                    boundRect=boundingRect(contours[i]);
                }
            }
        }
    } 

    if (maxArea==0){
        command.request.command="LEFT";
        commandClient.call(command);
    } else{
        rectangle(ptr, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        if ((boundRect.tl().x+boundRect.br().x)/2<leftBound) {
            command.request.command="LEFT";
            commandClient.call(command);
        } else if ((boundRect.tl().x+boundRect.br().x)/2>rightBound) {
            command.request.command="RIGHT";
            commandClient.call(command);
        } else{
            ++currentFrame;
            if (currentFrame==changeFrame){
                status=1;
                ROS_INFO("I'm gonna get it!\n");
                currentFrame=0;
            }
        }
    }
}

void status1(Mat& ptr, Mat& HSV) {
    if (mode=="red"){
        inRange(HSV, redLow, redHigh, threshold_image_red);
        findContours(threshold_image_red, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="purple"){
        inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
        findContours(threshold_image_purple, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="blue"){
        inRange(HSV, blueLow, blueHigh, threshold_image_blue);
        findContours(threshold_image_blue, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="green"){
        inRange(HSV, greenLow, greenHigh, threshold_image_green);
        findContours(threshold_image_green, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    }

    double maxArea=0;

    // approximate contours to rects
    for (int i = 0; i < contours.size(); i++) { 
        if (contourArea(contours[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours[i])>maxArea){
                Moments mom;
                mom=moments(contours[i],true);
                if (mom.m01/mom.m00>upperBound){
                    maxArea=contourArea(contours[i]);
                    boundRect=boundingRect(contours[i]);
                }
            }
        }
    } 

    if (maxArea==0){
        command.request.command="FORWARD";
        commandClient.call(command);
    } else{
        rectangle(ptr, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        if ((boundRect.tl().x+boundRect.br().x)/2<leftBound) {
            command.request.command="LEFT";
            commandClient.call(command);
        } else if ((boundRect.tl().x+boundRect.br().x)/2>rightBound) {
            command.request.command="RIGHT";
            commandClient.call(command);
        } else {
            command.request.command="FORWARD";
            commandClient.call(command);
        }
    }
}

void status2(Mat& ptr, Mat& HSV) {
    if (mode=="red"){
        inRange(HSV, redLow, redHigh, threshold_image_red);
        findContours(threshold_image_red, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="purple"){
        inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
        findContours(threshold_image_purple, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="blue"){
        inRange(HSV, blueLow, blueHigh, threshold_image_blue);
        findContours(threshold_image_blue, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="green"){
        inRange(HSV, greenLow, greenHigh, threshold_image_green);
        findContours(threshold_image_green, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    }

    double maxArea=0;

    // approximate contours to rects
    for (int i = 0; i < contours.size(); i++) { 
        if (contourArea(contours[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours[i])>maxArea){
                Moments mom;
                mom=moments(contours[i],true);
                if (mom.m01/mom.m00<upperBound){
                    maxArea=contourArea(contours[i]);
                    boundRect=boundingRect(contours[i]);
                }
            }
        }
    } 

    if (maxArea==0){
        command.request.command="LEFT";
        commandClient.call(command);
    } else{
        rectangle(ptr, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        if ((boundRect.tl().x+boundRect.br().x)/2<leftBound) {
            command.request.command="LEFT";
            commandClient.call(command);
        } else if ((boundRect.tl().x+boundRect.br().x)/2>rightBound) {
            command.request.command="RIGHT";
            commandClient.call(command);
        } else {
            ++currentFrame;
            if (currentFrame==changeFrame){
                status=3;
                ROS_INFO("I find the %s destination, I'm gonna deliver it now!\n",mode.c_str());
                currentFrame=0;
            }
        }

    }
}

void status3(Mat& ptr, Mat& HSV) {
    if (mode=="red"){
        inRange(HSV, redLow, redHigh, threshold_image_red);
        findContours(threshold_image_red, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="purple"){
        inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
        findContours(threshold_image_purple, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="blue"){
        inRange(HSV, blueLow, blueHigh, threshold_image_blue);
        findContours(threshold_image_blue, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="green"){
        inRange(HSV, greenLow, greenHigh, threshold_image_green);
        findContours(threshold_image_green, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    }

    double maxArea=0;

    // approximate contours to rects
    for (int i = 0; i < contours.size(); i++) {
        if (contourArea(contours[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours[i])>maxArea){
                Moments mom;
                mom=moments(contours[i],true);
                if (mom.m01/mom.m00<upperBound){
                    maxArea=contourArea(contours[i]);
                    boundRect=boundingRect(contours[i]);
                }
            }
        }
    } 

    if (maxArea==0){
        ++currentFrame;
        if (currentFrame==changeFrame){
            status=4;
            ROS_INFO("I have arrived, please take your package!\n");
            currentFrame=0;
        }
    } else{
        rectangle(ptr, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        if ((boundRect.tl().x+boundRect.br().x)/2<leftBound) {
            command.request.command="LEFT";
            commandClient.call(command);
        } else if ((boundRect.tl().x+boundRect.br().x)/2>rightBound) {
            command.request.command="RIGHT";
            commandClient.call(command);
        } else {
            command.request.command="FORWARD";
            commandClient.call(command);
        }

    }
}

void status4(Mat& ptr, Mat& HSV) {
    if (mode=="red"){
        inRange(HSV, redLow, redHigh, threshold_image_red);
        findContours(threshold_image_red, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="purple"){
        inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
        findContours(threshold_image_purple, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="blue"){
        inRange(HSV, blueLow, blueHigh, threshold_image_blue);
        findContours(threshold_image_blue, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="green"){
        inRange(HSV, greenLow, greenHigh, threshold_image_green);
        findContours(threshold_image_green, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    }

    double maxArea=0;

    // approximate contours to rects
    for (int i = 0; i < contours.size(); i++) { 
        if (contourArea(contours[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours[i])>maxArea){
                Moments mom;
                mom=moments(contours[i],true);
                if (mom.m01/mom.m00<upperBound){
                    maxArea=contourArea(contours[i]);
                    boundRect=boundingRect(contours[i]);
                }
            }
        }
    } 

    if (maxArea==0) {
        command.request.command="BACKWARD";
        commandClient.call(command);
    } else{
        rectangle(ptr, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        command.request.command="BACKWARD";
        commandClient.call(command);
        ++currentFrame;
        if (currentFrame==3){
            status=5;
            ROS_INFO("I have finished the job, I'm gonna find another package now!\n");
            currentFrame=0;
        }
    }
}

void status5(Mat& ptr, Mat& HSV) {
    if (mode=="red"){
        inRange(HSV, redLow, redHigh, threshold_image_red);
        findContours(threshold_image_red, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="purple"){
        inRange(HSV, purpleLow, purpleHigh, threshold_image_purple);
        findContours(threshold_image_purple, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="blue"){
        inRange(HSV, blueLow, blueHigh, threshold_image_blue);
        findContours(threshold_image_blue, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    } else if (mode=="green"){
        inRange(HSV, greenLow, greenHigh, threshold_image_green);
        findContours(threshold_image_green, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    }

    double maxArea=0;

    // approximate contours to rects
    for (int i = 0; i < contours.size(); i++) { 
        if (contourArea(contours[i]) < MIN_AREA) {
            // do nothing
        } else {
            if (contourArea(contours[i])>maxArea){
                Moments mom;
                mom=moments(contours[i],true);
                if (mom.m01/mom.m00<upperBound){
                    maxArea=contourArea(contours[i]);
                    boundRect=boundingRect(contours[i]);
                }
            }
        }
    } 

    if (maxArea==0){
        ++currentFrame;
        if (currentFrame==changeFrame){
            status=0;
            umap[mode]=false;
            mode="undefined";
            currentFrame=0;
            command.request.command="RESET";
            commandClient.call(command);
        }
    } else{
        rectangle(ptr, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        command.request.command="LEFT";
        commandClient.call(command);
    }
}

void status6(Mat& ptr, Mat& HSV) {
    command.request.command="LEFT";
    commandClient.call(command);
    /*uint8_t songLength = 16;
    uint8_t notes[16] = { 67, 67, 66, 66, 65, 65, 66, 66,
                          67, 67, 66, 66, 65, 65, 66, 66 };
    float durations[songLength];
    for (int i = 0; i < songLength; i++) {
      durations[i] = 0.25;
    }
    robot->defineSong(0, songLength, notes, durations);
    usleep(1000000);
    robot->playSong(0);*/
}

void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cvtColor(cv_ptr->image, HSV, COLOR_BGR2HSV);

    if(status==0){
        if (mode=="undefined"){
            undefined(cv_ptr->image,HSV);
        } else if (mode=="red"){
            status0(cv_ptr->image,HSV);
        } else if (mode=="purple"){
            status0(cv_ptr->image,HSV);
        } else if (mode=="blue"){
            status0(cv_ptr->image,HSV);
        } else if (mode=="green"){
            status0(cv_ptr->image,HSV);
        }
    }
    else if (status==1){
        if (mode=="red"){
            status1(cv_ptr->image,HSV);
        } else if (mode=="purple"){
            status1(cv_ptr->image,HSV);
        } else if (mode=="blue"){
            status1(cv_ptr->image,HSV);
        } else if (mode=="green"){
            status1(cv_ptr->image,HSV);
        }
    }
    else if (status==2){
        if (mode=="red"){
            status2(cv_ptr->image,HSV);
        } else if (mode=="purple"){
            status2(cv_ptr->image,HSV);
        } else if (mode=="blue"){
            status2(cv_ptr->image,HSV);
        } else if (mode=="green"){
            status2(cv_ptr->image,HSV);
        }
    }
    else if (status==3){
        if (mode=="red"){
            status3(cv_ptr->image,HSV);
        } else if (mode=="purple"){
            status3(cv_ptr->image,HSV);
        } else if (mode=="blue"){
            status3(cv_ptr->image,HSV);
        } else if (mode=="green"){
            status3(cv_ptr->image,HSV);
        }
    }
    else if (status==4){
        if (mode=="red"){
            status4(cv_ptr->image,HSV);
        } else if (mode=="purple"){
            status4(cv_ptr->image,HSV);
        } else if (mode=="blue"){
            status4(cv_ptr->image,HSV);
        } else if (mode=="green"){
            status4(cv_ptr->image,HSV);
        }
    }
    else if (status==5){
        if (mode=="red"){
            status5(cv_ptr->image,HSV);
        } else if (mode=="purple"){
            status5(cv_ptr->image,HSV);
        } else if (mode=="blue"){
            status5(cv_ptr->image,HSV);
        } else if (mode=="green"){
            status5(cv_ptr->image,HSV);
        }
    } else {
    	status6(cv_ptr->image,HSV);
    }
    // Output modified video stream
    imagePub.publish(cv_ptr->toImageMsg());
}

int main( int argc, char** argv ){

    umap["red"]=true;
    umap["purple"]=true;
    umap["blue"]=true;
    umap["green"]=true;

    ros::init(argc, argv, "color_detection");
    ros::NodeHandle n;

    commandClient = n.serviceClient<rc_sort::Command>("command");
    collideServer = n.advertiseService("collide", collideCallback);

    namedWindow(OPENCV_WINDOW,CV_WINDOW_AUTOSIZE);

    // Subscribe to input video feed and publish output video feed
    image_transport::ImageTransport it(n);
    imageSub = it.subscribe("/usb_cam/image_raw", 1, imageCallback);
    imagePub = it.advertise("/image_converter/output_video", 1);

    ros::MultiThreadedSpinner spinner(4);
    spinner.spin();

    destroyWindow(OPENCV_WINDOW);

    return 0;
}
