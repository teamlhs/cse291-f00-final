#include "ros/ros.h"
#include <cstdio>
#include "ncurses.h"
#include "rc_sort/Control.h"
#include "std_srvs/Empty.h"

using namespace std;

int main(int argc, char **argv) {
    ros::init(argc, argv, "rc_sort_controller");
    ros::NodeHandle n;

    // Setup client to exit session.
    ros::ServiceClient exitClient = n.serviceClient<std_srvs::Empty>("exit");

    // Setup client to change control.
    ros::ServiceClient controlClient = n.serviceClient<rc_sort::Control>("control");

    string control = "STOP";

    initscr();
    cbreak();
    noecho();
    while(ros::ok()) {
        char ch = getch();
        if (ch == 27) { // skip esc
            getch(); // skip [
            switch (getch()) {
            case 'A':
                control = "FORWARD";
                break;
            case 'B':
                control = "BACKWARD";
                break;
            case 'C':
                control = "RIGHT";
                break;
            case 'D':
                control = "LEFT";
                break;
            }
        } else if (ch == ' ') {
            control = "STOP";
        } else if (ch == 's') {
            control = "SONG";
        } else if (ch == 'e') {
            std::cout << "Exiting..." << std::endl;

            // Call exit service.
            std_srvs::Empty exitSrv;
            exitClient.call(exitSrv);

            return 0;
        } else {
            fprintf(stderr, "\nUnrecognizable keystorke\n");
            continue;
        }

        // Setup the service request.
        rc_sort::Control srv;
        srv.request.control = control;

        printf("%s\n", control.c_str());

        // Call the service request.
        if (controlClient.call(srv)) {
            fprintf(stderr, "\nSuccessful control %s\n", control.c_str());
        } else {
            fprintf(stderr, "\nSomething goes wrong for control %s\n", control.c_str());
            return 1;
        }
    }

    endwin();
}
