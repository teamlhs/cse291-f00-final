#include "ros/ros.h"
#include "rc_sort/Control.h"
#include "rc_sort/Command.h"
#include "rc_sort/Collide.h"
#include <iostream>
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h>
#include <termios.h>
#include <time.h>   // time calls
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include <ca_msgs/Bumper.h>
#include <std_msgs/String.h>

using namespace std;

const double pi = acos(-1);
const double offset = -pi/180*0;
const double HZ = 60;

const double d_angle = pi/180*5;
const double d_dist = 0.05;

double movement_x = 0;
double movement_y = 0;
double rotation_z = pi/2;

tf::StampedTransform initial_transform;

bool firstMsg = true;
ros::ServiceClient collideClient;

class RobotDriver {
private:
    //! The node handle we'll be using
    ros::NodeHandle nh_;
    //! We will be publishing to the "cmd_vel" topic to issue commands
    ros::Publisher cmd_vel_pub_;
    ros::Subscriber bumper_sub_;
    //! We will be listening to TF transforms as well
    tf::TransformListener listener_;

public:
    RobotDriver(ros::NodeHandle &nh);
    bool driveOdom(bool forward, double distance);
    bool turnOdom(bool clockwise, double radians);
    void bumperCallback(const ca_msgs::Bumper::ConstPtr& msg);
    bool controlCallback(rc_sort::Control::Request &req, rc_sort::Control::Response &res);
    bool commandCallback(rc_sort::Command::Request &req, rc_sort::Command::Response &res);
    void update(bool rotationOnly);
    bool check();
    bool reset();
};

//! ROS node initialization
RobotDriver::RobotDriver(ros::NodeHandle &nh) {
    nh_ = nh;
    //set up the publisher for the cmd_vel topic
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    bumper_sub_ = nh.subscribe("bumper", 1000, &RobotDriver::bumperCallback, this);
   
    //wait for the listener to get the first message
    listener_.waitForTransform("base_footprint", "odom", 
      ros::Time(0), ros::Duration(1.0));
    try{
        listener_.lookupTransform("base_footprint", "odom", 
          ros::Time(0), initial_transform);
    } catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
    }
}

void RobotDriver::update(bool rotationOnly) {
    //wait for the listener to get the first message
    listener_.waitForTransform("base_footprint", "odom", 
      ros::Time(0), ros::Duration(1.0));

    //get current transform
    tf::StampedTransform current_transform;
    try {
        listener_.lookupTransform("base_footprint", "odom", 
          ros::Time(0), current_transform);
    } catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
        return;
    }

    //calculate relative transform
    tf::Transform relative_transform = 
          initial_transform.inverse() * current_transform;

    if (rotationOnly == false) {
    //update movement_x, movement_y
        double dist_moved = relative_transform.getOrigin().length();
        if (relative_transform.getOrigin().x() < 0) {
            movement_x += dist_moved * cos(rotation_z);
            movement_y += dist_moved * sin(rotation_z);
        } else {
            movement_x -= dist_moved * cos(rotation_z);
            movement_y -= dist_moved * sin(rotation_z);
        }
        //ROS_INFO("%f %f\n", relative_transform.getOrigin().x(), 
          //relative_transform.getOrigin().y());
        //ROS_INFO("%f %f %f\n", dist_moved, cos(rotation_z), sin(rotation_z));
    }

    //update rotation_z
    tf::Vector3 actual_turn_axis = relative_transform.getRotation().getAxis();
    double angle_turned = relative_transform.getRotation().getAngle();
    //ROS_INFO("angle turned: %f, axis %f\n", angle_turned, actual_turn_axis.z());
    if (actual_turn_axis.z() < 0) {
        rotation_z += angle_turned;
    } else {
        rotation_z -= angle_turned;
    } 
    while(rotation_z < 0) rotation_z += 2*pi;
    while(rotation_z > 2*pi) rotation_z -= 2*pi;

    //update initial transform
    initial_transform = current_transform;
}

//! Drive a specified distance based on odometry information
bool RobotDriver::driveOdom(bool forward, double distance) {
    //wait for the listener to get the first message
    listener_.waitForTransform("base_footprint", "odom", 
      ros::Time(0), ros::Duration(1.0));

    //we will record transforms here
    tf::StampedTransform start_transform;
    tf::StampedTransform current_transform;

    //record the starting transform from the odometry to the base frame
    listener_.lookupTransform("base_footprint", "odom", 
      ros::Time(0), start_transform);

    //we will be sending commands of type "twist"
    geometry_msgs::Twist base_cmd;
    //the command will be to go forward at 0.25 m/s
    base_cmd.linear.y = base_cmd.angular.z = 0;
    if (forward == true) {
        base_cmd.linear.x = 0.1;
    } else {
        base_cmd.linear.x = -0.1;
    }

    ros::Rate rate(HZ);
    bool done = false;
    while (!done && nh_.ok()) {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try {
            listener_.lookupTransform("base_footprint", "odom", 
              ros::Time(0), current_transform);
        } catch (tf::TransformException ex) {
            ROS_ERROR("%s",ex.what());
            break;
        }
        //see how far we've traveled
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        double dist_moved = relative_transform.getOrigin().length();

        if(dist_moved > distance) done = true;
    }

    update(false);

    if (done) return true;
    return false;
}

bool RobotDriver::turnOdom(bool clockwise, double radians) {
    while(radians < 0) radians += 2*pi;
    while(radians > 2*pi) radians -= 2*pi;

    //tune radians with offset
    radians += offset;

    //wait for the listener to get the first message
    listener_.waitForTransform("base_footprint", "odom", 
      ros::Time(0), ros::Duration(1.0));

    //we will record transforms here
    tf::StampedTransform start_transform;
    tf::StampedTransform current_transform;

    //record the starting transform from the odometry to the base frame
    listener_.lookupTransform("base_footprint", "odom", 
      ros::Time(0), start_transform);

    //we will be sending commands of type "twist"
    geometry_msgs::Twist base_cmd;
    //the command will be to turn at 0.75 rad/s
    base_cmd.linear.x = base_cmd.linear.y = 0.0;
    base_cmd.angular.z = 0.1;
    if (clockwise) base_cmd.angular.z = -base_cmd.angular.z;

    //the axis we want to be rotating by
    tf::Vector3 desired_turn_axis(0,0,1);
    if (!clockwise) desired_turn_axis = -desired_turn_axis;

    ros::Rate rate(HZ);
    bool done = false;
    while (!done && nh_.ok()) {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try {
            listener_.lookupTransform("base_footprint", "odom", 
              ros::Time(0), current_transform);
        } catch (tf::TransformException ex) {
            ROS_ERROR("%s",ex.what());
            break;
        }
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        tf::Vector3 actual_turn_axis = 
          relative_transform.getRotation().getAxis();
        double angle_turned = relative_transform.getRotation().getAngle();
        if ( fabs(angle_turned) < 1.0e-2) continue;

        if ( actual_turn_axis.dot( desired_turn_axis ) < 0 ) 
            angle_turned = 2 * pi - angle_turned;

        if (angle_turned > radians) done = true;
    }

    update(true);

    if (done) return true;
    return false;
}

bool RobotDriver::reset() {
    //rotate to correct angle
    double initial_theta = atan2(movement_y, movement_x); // [-pi, +pi]
    double reverse_theta = initial_theta + pi; // [0, 2pi]
    double relative_theta = rotation_z - reverse_theta;
    turnOdom(true, relative_theta);

    //move to original position
    double dis = sqrt(movement_x * movement_x + movement_y * movement_y);
    driveOdom(true, dis);

    relative_theta = reverse_theta - pi/2;
    //ROS_INFO("%f\n", relative_theta);
    turnOdom(true, relative_theta);
    return true;
}

bool RobotDriver::check() {
    tf::StampedTransform current_transform;
    try {
        listener_.lookupTransform("base_footprint", "odom", 
          ros::Time(0), current_transform);
    } catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
        return false;
    }

    //see the relative transform from initial to current
    tf::Transform relative_transform = 
      initial_transform.inverse() * current_transform;

    //see the movement_x, movement_y, rotation_z
    //ROS_INFO("%f %f %f\n", movement_x, movement_y, rotation_z);

    return true;
}

bool RobotDriver::controlCallback(rc_sort::Control::Request &req, rc_sort::Control::Response &res) {
    string control = req.control;
    if (control=="FORWARD") {
        driveOdom(true, 0.25);
        check(); 
    } else if (control=="BACKWARD") {
        driveOdom(false, 0.25);
        check(); 
    } else if (control=="LEFT") {
        turnOdom(false, pi/2);
        check(); 
    } else if (control=="RIGHT") {
        turnOdom(true, pi/2);
        check(); 
    } else if (control=="STOP") {
        // output current position and angle
        check(); 
    } else if (control=="SONG") {
        // return to the original point
        reset();
    } else {	
        return false;
    }	
    return true;
}

bool RobotDriver::commandCallback(rc_sort::Command::Request &req, rc_sort::Command::Response &res) {
    string command = req.command;
    if (command == "LEFT") {
        turnOdom(false, d_angle);
        check(); 
    } else if (command == "RIGHT") {
        turnOdom(true, d_angle);
        check(); 
    } else if (command == "FORWARD") {
        driveOdom(true, d_dist);
        check(); 
    } else if (command == "BACKWARD") {
        driveOdom(false, d_dist);
        check(); 
    } else if (command == "RESET") {
        firstMsg = true;
	ROS_INFO("Reset collide flag\n");
    } else {
        return false;
    }
    return true;
}

void RobotDriver::bumperCallback(const ca_msgs::Bumper::ConstPtr& msg) {
    //bool collide = msg->is_left_pressed | msg->is_right_pressed |
    //  msg->is_light_left | msg->is_light_front_left | msg->is_light_center_left | 
    //  msg->is_light_center_right | msg->is_light_front_right | msg->is_light_right;
    bool collide =  msg->is_light_center_left | msg->is_light_center_right;

    if (collide == true) {
        if (firstMsg == true) {
            firstMsg = false;

            // Setup the service request.
            rc_sort::Collide srv;
            srv.request.collide = collide;
           
            // Call the service request.
            
            ROS_INFO("\nPrepare to send the message\n");
            if (collideClient.call(srv)) {
                ROS_INFO("\nSuccessfully send collide\n");
            } else {
                ROS_INFO("\nSomething goes wrong\n");
            }
            
        }

        //ROS_INFO("%d %d\n%d %d %d %d %d %d\n", msg->is_left_pressed, msg->is_right_pressed,
          //msg->is_light_left, msg->is_light_front_left, msg->is_light_center_left, 
          //msg->is_light_center_right, msg->is_light_front_right, msg->is_light_right);
    }
}

int main(int argc, char** argv) {
    //init the ROS node
    ros::init(argc, argv, "rc_sort_driver");
    ros::NodeHandle nh;
    //collidePub = nh.advertise<std_msgs::String>("/collide", 1);
    collideClient = 
      nh.serviceClient<rc_sort::Collide>("collide");

    //init transform
    RobotDriver driver(nh);
    ros::ServiceServer controlService = nh.advertiseService("control", 
      &RobotDriver::controlCallback, &driver);
    ros::ServiceServer commandService = nh.advertiseService("command", 
      &RobotDriver::commandCallback, &driver);

    while(ros::ok()) {
        ros::spin();
    }
}
